module ru.luminitetime.calculatorfx {
    requires javafx.controls;
    requires javafx.fxml;

    requires com.dlsc.formsfx;

    opens ru.luminitetime.calculatorfx to javafx.fxml;
    exports ru.luminitetime.calculatorfx;
}