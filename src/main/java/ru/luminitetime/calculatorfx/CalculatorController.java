package ru.luminitetime.calculatorfx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class CalculatorController {
    @FXML
    private Label resultText;
    static String a = "", operation = "", b = "";

    @FXML
    protected void onButtonClick(ActionEvent event) {
        Button btn = (Button) event.getSource();
        String text = btn.getText();
        if (text.equals("C")) {
            a = operation = b = "";
            resultText.setText("");
        } else if (text.charAt(0) >= '0' && text.charAt(0) <= '9') {
            if (operation.equals("")) {
                a += text;
                resultText.setText(a);
            } else {
                b += text;
                resultText.setText(a + " " + operation + " " + b);
            }
        } else if (text.equals("=")) {
            if (b.equals("")) {
                return;
            }
            String calculatedResult = calculateResult();
            resultText.setText(calculatedResult);
            if (calculatedResult.equals(new CalculationException().getMessage())) {
                a = "";
            } else {
                a = calculatedResult;
            }
            operation = b = "";
        } else if (operation.equals("") && !a.equals("")) {
            operation = text;
            resultText.setText(a + " " + operation);
        }
    }

    private static String calculateResult() {
        String result;
        switch (operation) {
            case "+" -> result = String.valueOf(Float.parseFloat(a) + Float.parseFloat(b));
            case "-" -> result = String.valueOf(Float.parseFloat(a) - Float.parseFloat(b));
            case "*" -> result = String.valueOf(Float.parseFloat(a) * Float.parseFloat(b));
            case "/" -> {
                try {
                    if (b.equals("0")) {
                        throw new CalculationException();
                    }
                    result = String.format("%.5f", Float.parseFloat(a) / Float.parseFloat(b));
                } catch (Exception e) {
                    return e.getMessage();
                }
            }
            default -> result = "";
        }
        return result;
    }
}

class CalculationException extends Exception {
    @Override
    public String getMessage() {
        return "Error!";
    }
}